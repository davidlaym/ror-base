# ROR-BASE

This repo contains files to start a vagrant box with anything necesary to develop Ruby and Rails.

This was first built according to the instructions in https://gorails.com/guides/using-vagrant-for-rails-development
Before provisioning the machine, ensure you have the following plugins for vagrant installed:

 - **vagrant-vbguest** automatically installs the host's VirtualBox Guest Additions on the guest system.
 - **vagrant-librarian-chef** let's us automatically run chef when we fire up our machine.
 
If not, please install them using the following commands on your host.

~~~
$ vagrant plugin install vagrant-vbguest
$ vagrant plugin install vagrant-librarian-chef-nochef
~~~

After the initial provisioning: run the following commands on the guest:

~~~
$ cd /vagrant
$ gem install bundler
$ gem install rails -v 4.2.1
$ rbenv rehash
$ bundle
~~~

**Note for rails 4.2 and up**
As noted by this [stackoverflow question and answers](http://stackoverflow.com/questions/27799260/rails-4-2-server-port-forwarding-on-vagrant-does-not-work)

To raise the rails server, use this command line:

~~~
$ rails server -b 0.0.0.0
~~~